var express = require('express')
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var http = require('http')

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api',require('./routes/api'))

//connect to our database
//Ideally you will obtain DB details from a config file
var dbName = 'RishCreationsDB';
var connectionString = 'mongodb://localhost:27017/' + dbName;

mongoose.connect(connectionString, function(err) {
    if(err) {
        console.log('connection error', err);
    } else {
        console.log('connection successful');
    }
});

module.exports = app;

app.listen(3000);
console.log('RishCreations API is LIVE.')