var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')
var urls = require('../util/enums').urls;
var transaction = require('../controllers/transaction');

router.route(urls.API.TRANSACTIONS)
	.get(transaction.getAll)
	.post(transaction.addNewTransaction);

router.route(urls.API.TRANSACTION_INFO)
	.get(transaction.findTransactionById);

router.delete(urls.API.TRANSACTION_DEL, function(req,res){
		console.log('I am in delete');
		res.send('give me something');
	});


router.get('/', function(req, res){
	res.send("Now we are in API.js")
})


module.exports = router;