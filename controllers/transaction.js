var Transaction = require('../models/transaction')

exports.getAll = function (req,res){
	Transaction.find(function(err, transactions) {
    if (err) {
      return res.send(err);
    }

    res.json(transactions);
  });
}

exports.addNewTransaction =  function (req, res){
	console.log(req.body);
	var transaction = processTransaction(req.body);
	transaction.save(function(err){
		if(err){
			return res.send(err);
		}
		res.send({message: "Added new Transaction."})
	});
}

exports.findByDropshipperID = function (req, res){
	res.send("findByDropshipperID.")
};

exports.findByAmazonID = function (req, res){
	res.send("findByAmazonID")
};

exports.updateTransaction = function (req, res){
	res.send("Update Transaction.")
};

exports.findTransactionById = function(req, res){
	console.log(req.query.id)
	Transaction.findOne({ _id: req.query.id}, function(err, transaction) {
   if (err) {
      return res.send(err);
    }

  res.json(transaction);
  });
}

exports.deleteTransaction = function (req, res){
	console.log('am here');
	 /*Transaction.remove({_id: req.query.id}, function(err, transaction) {
	    if (err) {
    	  return res.send(err);
    	}
    res.json({ message: 'Successfully deleted' });
  }); */
};



function processTransaction(_req){
	var transaction = new Transaction(_req);
	transaction.amazon_fee = _req.amazon_list_price * .15;
	transaction.item_earnings = _req.amazon_list_price - transaction.amazon_fee;
	transaction.margin_gain = (transaction.item_earnings*66.17) - transaction.lavish_price_INR ;
	return transaction;
}