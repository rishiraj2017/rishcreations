var urls = {
	API: {
		HOME: '/',
		TRANSACTIONS: '/transactions',
		TRANSACTION_INFO: '/transactioninfo',
		TRANSACTION_DEL: '/transactiondelete/:id'
	}
}

exports.urls = urls;