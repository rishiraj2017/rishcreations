var mongoose = require('mongoose')

Schema = mongoose.Schema;


var InvoiceSchema = new Schema({
	invoice_id: String,
	period_start: Date,
	period_end: Date,
	start_order_id: String,
	end_order_id: String,
	amazon_payout_USD: Number,
	amazon_payout_INR: Number,
	marketplace: String,
	exchange: Number,

});