var mongoose = require('mongoose')

Schema = mongoose.Schema;

var TransactionSchema = new Schema({
	order_id_lavish: String,
	order_id_amazon: String,
	amazon_list_price: Number,
	amazon_fee: Number,
	item_earnings: Number,
	lavish_price_INR: Number,
	market_place: String, 
	margin_gain: Number,
});


module.exports = mongoose.model('Transaction', TransactionSchema);